import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriorityComponent } from './priority.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PriorityComponent]
})
export class PriorityModule { }
