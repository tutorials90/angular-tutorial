import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { PriorityStore, PriorityState } from './priority.store';
import { Priority } from './priority.model';

@Injectable({
  providedIn: 'root'
})
export class PriorityQuery extends QueryEntity<PriorityState, Priority> {

  constructor(protected store: PriorityStore) {
    super(store);
  }

}
