export interface Priority {
  id: string;
  name: string;
  value: number;
  color: string;
}

/**
 * A factory function that creates Priority
 */
export function createPriority(params: Partial<Priority>) {
  return {} as Priority;
}
