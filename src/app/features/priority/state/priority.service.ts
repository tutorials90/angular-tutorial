import { Injectable } from '@angular/core';
import { PriorityStore } from './priority.store';
import { Priority } from './priority.model';

@Injectable({ providedIn: 'root' })
export class PriorityService {
  constructor(private store: PriorityStore) { }

  load(filter = {}) {
    this.store.upsertMany(
      [
        {
          "id": "722859eb-28e3-4838-a76f-3ae443a45cf4",
          "name": "High",
          "value": 3,
          "color": "#FF9800"
        },
        {
          "id": "a8669ce8-7e52-4ff9-b52e-607c15ec4b51",
          "name": "Urgent",
          "value": 4,
          "color": "#ff5722"
        },
        {
          "id": "8d8cb1ea-ee63-410b-a368-b0ecc3dca840",
          "name": "Low",
          "value": 1,
          "color": "#566068"
        },
        {
          "id": "ca74faf6-6d86-11e8-922e-1f8dbe8e60f2",
          "name": "-",
          "value": 0,
          "color": "#566068"
        },
        {
          "id": "75c09565-23e2-43ae-9751-b22c87ff8301",
          "name": "Medium",
          "value": 2,
          "color": "#49a7a5"
        }
      ]
    );
  }

  create(priority: Priority) {
    this.store.add(priority);
  }

  updateOne(priority: Partial<Priority>) {
    this.store.upsert(priority.id, priority);
  }

  delete(id: string) {
    this.store.remove(id);
  }
}
