import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Priority } from './priority.model';

export interface PriorityState extends EntityState<Priority> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'priority' })
export class PriorityStore extends EntityStore<PriorityState, Priority> {

  constructor() {
    super();
  }

}

