import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Priority } from 'src/app/features/priority/state/priority.model';
import { PriorityQuery } from 'src/app/features/priority/state/priority.query';
import { PriorityService } from 'src/app/features/priority/state/priority.service';
import { ConfirmationDialogComponent, ConfirmDialogData } from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import { AddPriorityDialogComponent } from './components/add-priority-dialog/add-priority-dialog.component';

@Component({
  selector: 'app-priority-settings',
  templateUrl: './priority-settings.component.html',
  styleUrls: ['./priority-settings.component.scss']
})
export class PrioritySettingsComponent implements OnInit, OnDestroy {

  @Input() name: string = "dede";

  selectedPriorities;

  priorities$: Observable<Priority[]> = this.priorityQuery.selectAll()
    .pipe(
      map(
        priorities => priorities.map(p => ({ ...p }))
      )
    );

  priorities: Priority[] = [];

  subscribtions = new Subscription();

  constructor(
    private priorityService: PriorityService,
    private priorityQuery: PriorityQuery,
    private dialog: MatDialog
  ) { }

  ngOnDestroy(): void {
    this.subscribtions.unsubscribe();
  }

  ngOnInit() {
    this.priorityService.load();

    this.subscribtions.add(this.priorities$.subscribe(
      objects => this.priorities = objects
    ));
  }

  onEditComplete(event) {
    this.priorityService.updateOne(event.data);
  }

  addPriority() {
    // open dialog
    this.dialog
      .open(AddPriorityDialogComponent, {
        width: '350px'
      })
      .afterClosed()
      .pipe(take(1))
      .subscribe();
  }

  deletePriority(priority: Priority) {
    // confirm delete
    this.dialog
      .open(ConfirmationDialogComponent, {
        data: {
          message: 'Do you really want to remove this priority?',
          confirmText: 'Delete'
        } as ConfirmDialogData
      })
      .afterClosed()
      .pipe(take(1)) // take first emit
      .subscribe(res => {
        if (res) {
          this.priorityService.delete(priority.id);
        }
      });
  }
}
