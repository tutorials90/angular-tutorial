import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Priority } from 'src/app/features/priority/state/priority.model';
import { PriorityService } from 'src/app/features/priority/state/priority.service';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-add-priority-dialog',
  templateUrl: './add-priority-dialog.component.html',
  styleUrls: ['./add-priority-dialog.component.scss']
})
export class AddPriorityDialogComponent implements OnInit {

  form!: FormGroup;
  
  constructor(
    public dialogRef: MatDialogRef<AddPriorityDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private priorityService: PriorityService
  ) { }

  
  ngOnInit() {
    this.initForm();
  }

  submit() {
    const formData = this.form.value;

    const newPriority: Priority = {
      id: uuid(),
      name: formData.name,
      color: formData.color,
      value: formData.value,
    };

    this.priorityService.create(newPriority);

    this.dialogRef.close();
  }

  cancel() {
    this.dialogRef.close();
  }

  private initForm(): void {

    this.form = this.fb.group({
      name: ["New Priority", [Validators.required]],
      color: ["#fff", [Validators.required]],
      value: [0, [Validators.required]]
    });
  }

}