import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DesignModule } from "./design/design.module";
import { PrioritySettingsComponent } from "./components/priority-settings/priority-settings.component";
import { ConfirmationDialogComponent } from "./dialogs/confirmation-dialog/confirmation-dialog.component";
import { AddPriorityDialogComponent } from "./components/priority-settings/components/add-priority-dialog/add-priority-dialog.component";


const PIPES: any[] = [];

const MODULES: any[] = [
  DesignModule
];

const COMPONENTS: any[] = [
  PrioritySettingsComponent
];

const DIRECTIVES: any[] = [];

const DIALOGS: any[] = [
  ConfirmationDialogComponent,
  AddPriorityDialogComponent
];

@NgModule({
  declarations: [...PIPES, ...DIRECTIVES, ...COMPONENTS, ...DIALOGS],
  imports: [CommonModule, ...MODULES],
  entryComponents: [...DIALOGS],
  exports: [...PIPES, ...MODULES, ...DIRECTIVES, ...COMPONENTS, ...DIALOGS]
})
export class SharedModule { }
